package com.eendtech.www.cqcet_eschool;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Seale [QQ:1801157108] on 2018/4/28.
 */

public class GradeSelectFragment extends android.support.v4.app.Fragment {
    private List<String> ClassName;
    private List<String> ClassKind;
    private List<String> Grade;
    private RecyclerView rV_GradeSelect;
    private MyGradeRecyclerViewAdapter myGradeRecyclerViewAdapter;
    private TextView tv_grade_userName;
    final String SELECT_GRADE = "http://42.247.8.141:1590/Achievement";
    private TextView tv_grade_time;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gradeselect_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_grade_time = view.findViewById(R.id.tv_grade_time);
        rV_GradeSelect = view.findViewById(R.id.rV_GradeSelect);
        tv_grade_userName = view.findViewById(R.id.tv_grade_userName);

        getUserName();
        initData();
        getDate();
        myGradeRecyclerViewAdapter = new MyGradeRecyclerViewAdapter(getContext(),ClassName,ClassKind,Grade);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rV_GradeSelect.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        rV_GradeSelect.addItemDecoration(new DividerItemDecoration(getContext(),1));
        rV_GradeSelect.setAdapter(myGradeRecyclerViewAdapter);
        rV_GradeSelect.setItemAnimator(new DefaultItemAnimator());


    }


    private void initData() {
        ClassName = new ArrayList<String>();
        ClassKind = new ArrayList<String>();
        Grade = new ArrayList<String>();

        OkGo.<String>get(SELECT_GRADE)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Document document = Jsoup.parse(response.body());
                        Elements trs = document.getElementsByClass("Content").get(0).select("tr");
                        for (int i = 3 ; i < trs.size() ;i++){
                            Elements tds = trs.get(i).select("td");
                            ClassName.add(tds.get(0).text());
                            ClassKind.add(tds.get(1).text());
                            Grade.add(tds.get(3).text());
                        }
                    }
                });








    }


    private void getDate(){
        String dateDatas;
        //获取时间
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateDatas = simpleDateFormat.format(date);
        tv_grade_time.setText(dateDatas);
    }


    private void getUserName(){
        OkGo.<String>get(SELECT_GRADE)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Document document = Jsoup.parse(response.body());
                        Elements Content = document.select("div.Content");

                        //获取姓名
                        Elements Name = Content.select("div[style=\"width:100px;\"]");
                        String temp ;
                        temp = Name.text();
                        temp = temp.replace("姓名：","");
                        tv_grade_userName.setText("亲爱的"+temp+"同学");
                    }
                });

    }
}
