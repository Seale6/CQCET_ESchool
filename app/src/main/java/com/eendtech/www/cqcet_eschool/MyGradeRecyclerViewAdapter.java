package com.eendtech.www.cqcet_eschool;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Seale [QQ:1801157108] on 2018/5/6.
 */

public class MyGradeRecyclerViewAdapter extends RecyclerView.Adapter<MyGradeRecyclerViewAdapter.MyViewHolder> {

    private List<String> ClassName;
    private List<String> ClassKind;
    private List<String> Grade;

    private Context mContext ;
    private LayoutInflater inflater;

    public MyGradeRecyclerViewAdapter(Context mContext , List<String> ClassName,List<String> ClassKind,List<String> Grade){
        this.mContext = mContext;
        this.ClassName = ClassName;
        this.ClassKind = ClassKind;
        this.Grade = Grade;

        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyGradeRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recyclerview_grade_item,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyGradeRecyclerViewAdapter.MyViewHolder holder, int position) {
        holder.tv_item_grade_xk.setText(ClassName.get(position));
        holder.tv_item_grade_lx.setText(ClassKind.get(position));
        holder.tv_item_grade.setText(Grade.get(position));

    }

    @Override
    public int getItemCount() {
        return ClassName.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_item_grade_xk , tv_item_grade_lx , tv_item_grade;


        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_grade = itemView.findViewById(R.id.tv_item_grade);
            tv_item_grade_lx = itemView.findViewById(R.id.tv_item_grade_lx);
            tv_item_grade_xk = itemView.findViewById(R.id.tv_item_grade_xk);



        }
    }
}
