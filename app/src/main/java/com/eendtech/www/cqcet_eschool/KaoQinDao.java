//package com.eendtech.www.cqcet_eschool;
//
//import android.util.Log;
//
////import com.zcq.app.ecard.model.bean.KaoQin;
////import com.zcq.app.ecard.model.bean.Record;
////import com.zcq.app.ecard.model.bean.ScoreBean;
////import com.zcq.app.ecard.utils.OkHttpUtil;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.select.Elements;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import okhttp3.FormBody;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.RequestBody;
//import okhttp3.Response;
//
///**
// * Created by Mr.z on 2018/5/4.
// */
//
//public class KaoQinDao {
//
//    private static String LOGIN_KAOQ_URL = "http://42.247.8.141:1590/api/ajax/Login";
////    private static OkHttpClient okHttpClient = OkHttpUtil.getKaoQinInstance();
//
//    private static String KAOQIN_KQ = "http://42.247.8.141:1590/kq";
//
//    private static final String SCORE_URL = "http://42.247.8.141:1590/Achievement";
//
//    //登录考勤系统
//    public static boolean loginKaoQin(String user) throws IOException {
//
//        RequestBody requestBodyPost = new FormBody.Builder()
//                .add("LoginName",user)
//                .add("LoginPwd",user)
//                .build();
//        Request request = new Request.Builder().url(LOGIN_KAOQ_URL).removeHeader("User-Agent")
//                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
//                .post(requestBodyPost).build();
////        Response rsp = okHttpClient.newCall(request).execute();
////        String data = rsp.body().string();
//
//        JSONObject jsonObject = null;
//        try {
////            jsonObject = new JSONObject(data);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        String message = jsonObject.optString("Message");
//        Log.d("loginmsg",message);
//        if("success".equals(message))
//            return true;
//        else
//            return false;
//    }
//
//    public static List<KaoQin> queryKq(String state, String dateTime){
//
//        String url = KAOQIN_KQ + "?State="+state + "&dateTime="+dateTime;
//        Request request = new Request.Builder().url(url).removeHeader("User-Agent")
//                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
//                .get().build();
//        Response rsp = null;
//
//        try {
//
//            List<KaoQin> kaoQins = new ArrayList<>();
//
////            rsp = okHttpClient.newCall(request).execute();
//            String data = rsp.body().string();
//
//            Document doc = Jsoup.parse(data);
//            Elements trs = null;
//            try {
//                trs = doc.getElementsByClass("Content").get(0).select("tr");
//            }catch (NullPointerException e){
//                e.printStackTrace();
//            }
//
//            for (int i = trs.size()-1;i > 0;i--){
//
//                Elements tds = trs.get(i).select("td");
//                KaoQin kq = new KaoQin();
//
//                kq.setName(tds.get(0).text());
//                kq.setNum(tds.get(1).text());
//                kq.setTeacher(tds.get(2).text());
//                kq.setDate(tds.get(3).text());
//                kq.setClasses(tds.get(4).text());
//                kq.setState(tds.get(5).text());
//
//
//                kaoQins.add(kq);
//
//            }
//            return kaoQins;
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public static Map<String,List<ScoreBean>> queryScore(){
//
//        Request request = new Request.Builder().url(SCORE_URL).removeHeader("User-Agent")
//                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
//                .get().build();
//        Response rsp = null;
//
//        try {
////            rsp = okHttpClient.newCall(request).execute();
//            String data = rsp.body().string();
//
//            Document doc = Jsoup.parse(data);
//            Elements trs = null;
//            try {
//                trs = doc.getElementsByClass("Content").get(0).select("tr");
//            }catch (NullPointerException e){
//                e.printStackTrace();
//            }
//
//            Map<String,List<ScoreBean>> map = new HashMap<>();
//            List<ScoreBean> scores = null;
//            for (int i = 1;i < trs.size();i++){
//
//                Elements ths = trs.get(i).select("th");
//                if(ths.size()==1){
//                    String name = ths.get(0).text();
//                    scores = new ArrayList<>();
//                    map.put(name,scores);
//                }
//
//                Elements tds = trs.get(i).select("td");
//                if(tds.size()==0)
//                    continue;
//                ScoreBean score = new ScoreBean();
//                score.setScoreName(tds.get(0).text());
//                score.setScoreNature(tds.get(1).text());
//                score.setScoreCredit(tds.get(2).text());
//                score.setScoreGarde(tds.get(5).text());
//                score.setScoreResult(tds.get(3).text());
//                score.setNowScore(tds.get(7).text());
//                score.setTermScore(tds.get(8).text());
//
//                scores.add(score);
//
//            }
//            return map;
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//
//    }
//
//}
