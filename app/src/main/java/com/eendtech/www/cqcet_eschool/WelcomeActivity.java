package com.eendtech.www.cqcet_eschool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.Callback;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.cookie.store.CookieStore;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Progress;
import com.lzy.okgo.model.Response;
import com.lzy.okgo.request.base.Request;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;

import okhttp3.Cookie;
import okhttp3.HttpUrl;


public class WelcomeActivity extends AppCompatActivity {

    String select_Grade = "http://42.247.8.141:1590/Achievement";
    String select_KQ = "http://42.247.8.141:1590/KQ";



    Button send_request;
    TextView response_text;
    Button btn_Get;
    Button btn_getCookies;
    Button btn_Jsoup;









    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);


        send_request = (Button) findViewById(R.id.send_request);
        response_text = (TextView) findViewById(R.id.response_text);
        btn_Get = (Button)findViewById(R.id.btn_Get) ;
        btn_getCookies = (Button) findViewById(R.id.btn_getCookies);
        btn_Jsoup = (Button) findViewById(R.id.btn_Jsoup);



        send_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postHttp("http://42.247.8.141:1590/api/ajax/Login","2017180543","2017180543");

            }
        });

        btn_Get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHttp("http://42.247.8.141:1590/KQ");
            }
        });

        btn_getCookies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                response_text.setText(getCookie("http://42.247.8.141:1590/api/ajax/Login"));
            }
        });
        btn_Jsoup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getUsrInfo();



            }
        });



    }










    protected void postHttp(String URL , String LoginName , String LoginPwd){
        OkGo.<String>post(URL)
                .tag(this)
                .params("LoginName",LoginName)
                .params("LoginPwd",LoginPwd)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        response_text.setText(response.body());
                        response_text.setText(response.headers().get("Set-Cookie"));
                    }
                });



    }

    protected void getHttp(final String URL){
        OkGo.<String>get(URL)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Document document = Jsoup.parse(response.body());

                        response_text.setText(response.body());



                    }
                });
    }




    protected String getCookie(String URL){
        CookieStore cookieStore = OkGo.getInstance().getCookieJar().getCookieStore();
        HttpUrl httpUrl = HttpUrl.parse(URL);
        List<Cookie> cookies = cookieStore.getCookie(httpUrl);
        return cookies.get(0).value().toString();


    }


    protected void getUsrInfo(){
        OkGo.<String>get(select_Grade)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Document doc = Jsoup.parse(response.body());
                        Elements StudentNumber = doc.select("div.Content");
                        Element l1 = StudentNumber.select("div.l").first();
                        String temp ;
                        temp = l1.text();
                        response_text.setText(temp);

                    }
                });

    }

















}



