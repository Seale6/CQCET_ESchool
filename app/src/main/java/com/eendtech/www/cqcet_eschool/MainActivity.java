package com.eendtech.www.cqcet_eschool;


import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.item.*;
import me.majiajie.pagerbottomtabstrip.item.NormalItemView;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectedListener;

public class MainActivity extends AppCompatActivity {
    private MyFragmentPagerAdapter myFragmentPagerAdapter;
    private MyFragment myFragment;
    private KQSelectFragment kqSelectFragment;
    private GradeSelectFragment gradeSelectFragment;
    private List<Fragment> fragmentList = new ArrayList<Fragment>();
    private ViewPager viewPager;
    private TextView tv_TopTab;

    PageNavigationView BottomTab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();






    }

    private void initView(){

        BottomTab = (PageNavigationView)findViewById(R.id.BottomTab);
        tv_TopTab = (TextView)findViewById(R.id.tv_TopTab);
        NavigationController navigationController = BottomTab.custom()
                .addItem(newItem(R.drawable.grade_normal,R.drawable.grade_check,"成绩查询"))
                .addItem(newItem(R.drawable.kqselect_normal,R.drawable.kqselect_check,"考勤查询"))
                .addItem(newItem(R.drawable.my_normal,R.drawable.my_check,"我的"))
                .build();

        viewPager = (ViewPager)findViewById(R.id.viewpaper);
        myFragment = new MyFragment();
        kqSelectFragment = new KQSelectFragment();
        gradeSelectFragment = new GradeSelectFragment();

        fragmentList.add(gradeSelectFragment);
        fragmentList.add(kqSelectFragment);
        fragmentList.add(myFragment);
        myFragmentPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),fragmentList);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(myFragmentPagerAdapter);
        viewPager.setCurrentItem(0);//设置初始位置

        navigationController.setupWithViewPager(viewPager);





    }
    private BaseTabItem newItem(int drawable, int checkedDrawable, String text){
        me.majiajie.pagerbottomtabstrip.item.NormalItemView normalItemView = new NormalItemView(this);
        normalItemView.initialize(drawable,checkedDrawable,text);
        normalItemView.setTextDefaultColor(Color.GRAY);
        normalItemView.setTextCheckedColor(Color.parseColor("#FF8C00"));
        return normalItemView;
    }




}








