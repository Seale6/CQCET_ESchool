package com.eendtech.www.cqcet_eschool;

import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.Callback;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Progress;
import com.lzy.okgo.model.Response;
import com.lzy.okgo.request.base.Request;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import org.jsoup.select.Elements;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Seale [QQ:1801157108] on 2018/4/28.
 */

public class KQSelectFragment extends android.support.v4.app.Fragment {
    private RecyclerView rv_KQSelect;
    private List<String> ClassName;
    private List<String> ClassTeacher;
    private List<String> KQStatus;
    private MyRecyclerViewAdapter myRecyclerViewAdapter;
    private TextView tv_kq_date;
    private TextView tv_kq_userName,tv_count;

    final String POSTLOGIN = "http://42.247.8.141:1590/api/ajax/Login";
    private final String KQHTTP = "http://42.247.8.141:1590/KQ";
    final String SELECT_GRADE = "http://42.247.8.141:1590/Achievement";

    private String count ;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kqselect_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv_KQSelect = view.findViewById(R.id.rv_KQSelect);
        tv_kq_date = view.findViewById(R.id.tv_kq_date);
        tv_kq_userName = view.findViewById(R.id.tv_kq_userName);
        tv_count = view.findViewById(R.id.tv_count);

        getDate();
        initData();
        getUserName();


        myRecyclerViewAdapter= new MyRecyclerViewAdapter(getContext() , ClassName,ClassTeacher,KQStatus);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_KQSelect.setLayoutManager(layoutManager);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rv_KQSelect.addItemDecoration(new DividerItemDecoration(getContext(),1));
        rv_KQSelect.setAdapter(myRecyclerViewAdapter);
        rv_KQSelect.setItemAnimator(new DefaultItemAnimator());
        getCount();


    }
    private void initData() {

        ClassName = new ArrayList<String>();
        ClassTeacher = new ArrayList<String>();
        KQStatus = new ArrayList<String>();

        OkGo.<String>get(KQHTTP)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Document document = Jsoup.parse(response.body());
                        Elements trs = document.getElementsByClass("Content").get(0).select("tr");
                        for (int i = 1 ; i < trs.size()-1 ; i++){
                            Elements tds = trs.get(i).select("td");
                            ClassName.add(tds.get(4).text());
                            ClassTeacher.add(tds.get(2).text());
                            KQStatus.add(tds.get(5).text());
                        }
                    }
                });
                count = Integer.toString(ClassName.size());


    }

    private void getDate(){
        String dateDatas;
        //获取时间
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateDatas = simpleDateFormat.format(date);
        tv_kq_date.setText(dateDatas);
    }

    private void getUserName(){
        OkGo.<String>get(SELECT_GRADE)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Document document = Jsoup.parse(response.body());
                        Elements Content = document.select("div.Content");

                        //获取姓名
                        Elements Name = Content.select("div[style=\"width:100px;\"]");
                        String temp ;
                        temp = Name.text();
                        temp = temp.replace("姓名：","");
                        tv_kq_userName.setText("亲爱的"+temp+"同学");
                    }
                });

    }
    private void getCount(){

        tv_count.setText("提示:最新考勤记录由上往下排列");
    }






}





