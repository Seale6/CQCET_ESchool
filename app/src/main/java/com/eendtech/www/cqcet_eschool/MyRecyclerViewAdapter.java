package com.eendtech.www.cqcet_eschool;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.ColorRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Seale [QQ:1801157108] on 2018/5/6.
 * 考勤界面RecyclerViewAdapter
 */

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder>{

    private List<String> ClassName;
    private List<String> ClassTeacher;
    private List<String> KQStatus;
    private Context mContext ;
    private LayoutInflater inflater;

    public MyRecyclerViewAdapter(Context mContext , List<String> ClassName,List<String> ClassTeacher,List<String> KQStatus){
        this.mContext = mContext;
        this.ClassName = ClassName;
        this.ClassTeacher = ClassTeacher;
        this.KQStatus = KQStatus;
        inflater = LayoutInflater.from(mContext);

    }


    @Override
    public MyRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recyclerview_kq_item,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }
    //填充onCreateViewHolder方法返回的holder中的控件
    @Override
    public void onBindViewHolder(MyRecyclerViewAdapter.MyViewHolder holder, int position) {

        holder.tv_item_xk.setText(ClassName.get(position));

        holder.tv_item_teacher.setText(ClassTeacher.get(position));


        if (KQStatus.get(position).equals("正常")){
            holder.tv_item_kq.setText(KQStatus.get(position));
            holder.img_item_kq.setImageResource(R.mipmap.item_zc);
            holder.tv_item_kq.setTextColor(Color.parseColor("#3ac101"));
        }else if (KQStatus.get(position).equals("旷课")){
            holder.tv_item_kq.setText(KQStatus.get(position));
            holder.img_item_kq.setImageResource(R.mipmap.item_cw);
            holder.tv_item_kq.setTextColor(Color.parseColor("#c90021"));
        }
        else if (KQStatus.get(position).equals("迟到")){
            holder.tv_item_kq.setText(KQStatus.get(position));
            holder.img_item_kq.setImageResource(R.mipmap.item_cw);
            holder.tv_item_kq.setTextColor(Color.parseColor("#c90021"));
        }else if (KQStatus.get(position).equals("事假")){
            holder.tv_item_kq.setText(KQStatus.get(position));
            holder.img_item_kq.setImageResource(R.mipmap.item_qj);
            holder.tv_item_kq.setTextColor(Color.parseColor("#7c008f"));
        }else if (KQStatus.get(position).equals("公假")){
            holder.tv_item_kq.setText(KQStatus.get(position));
            holder.img_item_kq.setImageResource(R.mipmap.item_qj);
            holder.tv_item_kq.setTextColor(Color.parseColor("#7c008f"));
        }else {
            holder.tv_item_kq.setText(KQStatus.get(position));
            holder.img_item_kq.setImageResource(R.mipmap.item_qj);
            holder.tv_item_kq.setTextColor(Color.parseColor("#7c008f"));
        }
        if (ClassName.get(position).equals("毛泽东思想和中国特色社会主义理论体系概论")){
            holder.tv_item_xk.setText("毛概");
        }
    }

    @Override
    public int getItemCount() {
        return ClassName.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tv_item_xk,tv_item_teacher,tv_item_kq;
        ImageView img_item_kq,img_item_icon;



        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_xk = itemView.findViewById(R.id.tv_item_xk);
            tv_item_teacher = itemView.findViewById(R.id.tv_item_teacher);
            tv_item_kq = itemView.findViewById(R.id.tv_item_kq);
            img_item_icon = itemView.findViewById(R.id.img_item_icon);
            img_item_kq = itemView.findViewById(R.id.img_item_kq);
        }
    }
}
