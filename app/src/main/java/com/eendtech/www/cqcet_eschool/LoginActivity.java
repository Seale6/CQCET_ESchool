package com.eendtech.www.cqcet_eschool;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;

import java.util.zip.Inflater;

public class LoginActivity extends AppCompatActivity {
    CustomVideoView videoView ;
    RelativeLayout rl_userName,rl_userPwd;
    EditText et_UserName , et_UserPwd;
    ImageView iv_userClear , iv_pwdClear;
    Button btn_Login;
    final String POSTLOGIN = "http://42.247.8.141:1590/api/ajax/Login";
    private String UserName, Pwd;
    private Dialog LoadingDialog;
    private LinearLayout linearLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();







    }


    private void initView(){
//绑定控件
        videoView = (CustomVideoView)findViewById(R.id.videoView);
        rl_userName = (RelativeLayout)findViewById(R.id.rl_userName);
        rl_userPwd = (RelativeLayout)findViewById(R.id.rl_userPwd);
        et_UserName = (EditText)findViewById(R.id.et_UserName);
        et_UserPwd = (EditText)findViewById(R.id.et_UserPwd);
        iv_pwdClear = (ImageView)findViewById(R.id.iv_pwdClear);
        iv_userClear = (ImageView)findViewById(R.id.iv_userClear);
        btn_Login = (Button) findViewById(R.id.btn_Login) ;

//        设置加载中画面
        initLoadingView();




//        控件监听事件

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserName = et_UserName.getText().toString();
                Pwd = et_UserPwd.getText().toString();

                //登录考勤系统
                if (UserName.equals("") || Pwd.equals("")){
                    Toast.makeText(getApplication(),"账号或密码不能为空",Toast.LENGTH_LONG).show();
                }else {

//                    进入Dialog 加载中界面
                    LoadingDialog.show();
                    OkGo.<String>post(POSTLOGIN)
                            .tag(this)
                            .params("LoginName",UserName)
                            .params("LoginPwd",Pwd)
                            .execute(new StringCallback() {
                                @Override
                                public void onSuccess(Response<String> response) {
                                    LoadingDialog.dismiss();//取消显示加载界面
                                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                }









            }
        });

















//        -----------------------------

//       视频背景
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.cqcet_background));
//        播放
        videoView.start();
//        循环播放
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });


//        透明设置
//        布局透明
        rl_userName.setAlpha(0.65f);
        rl_userPwd.setAlpha(0.65f);
//         控件透明
        btn_Login.setAlpha(0.65f);



//        添加Tools类监听Clear按钮
        EditTextClearTools.addclerListenner(et_UserName,iv_userClear);
        EditTextClearTools.addclerListenner(et_UserPwd,iv_pwdClear);






    }
//返回重启加载
    protected void onRestart() {
        initView();
        super.onRestart();
    }

//    防止锁屏或者切出时音乐在播放

    protected void onStop() {
        videoView.stopPlayback();
        super.onStop();
    }

    //初始化加载中界面Dialog
    private void initLoadingView(){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_loading_style,null);
        linearLayout = view.findViewById(R.id.ll_loading_back);
        linearLayout.setAlpha(0.8f);

        LoadingDialog = new Dialog(LoginActivity.this,R.style.progress_dialog);
        LoadingDialog.setContentView(R.layout.dialog_loading_style);
        LoadingDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }





}
