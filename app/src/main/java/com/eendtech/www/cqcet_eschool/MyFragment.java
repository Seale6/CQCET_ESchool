package com.eendtech.www.cqcet_eschool;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.cookie.store.CookieStore;
import com.lzy.okgo.model.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.HttpUrl;


/**
 * Created by Seale [QQ:1801157108] on 2018/4/28.
 */

public class MyFragment extends android.support.v4.app.Fragment {
    final String SELECT_GRADE = "http://42.247.8.141:1590/Achievement";
    final String POSTLOGIN = "http://42.247.8.141:1590/api/ajax/Login";
    TextView info_name , info_school , info_number , info_class , info_xz;
    Button btn_Out;
    AppCompatActivity appCompatActivity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        info_name = (TextView)view.findViewById(R.id.info_name);
        info_class = (TextView)view.findViewById(R.id.info_class);
        info_number = (TextView)view.findViewById(R.id.info_number);
        info_school = (TextView)view.findViewById(R.id.info_school);
        info_xz = (TextView)view.findViewById(R.id.info_xz);
        btn_Out = (Button)view.findViewById(R.id.btn_Out);

        getInformationForHttp();

        btn_Out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                移除Cookie
                CookieStore cookieStore = OkGo.getInstance().getCookieJar().getCookieStore();
                cookieStore.removeAllCookie();
                //取得当前活动
                appCompatActivity = (AppCompatActivity)getActivity();
                Intent intent = new Intent(getActivity(),LoginActivity.class);
                startActivity(intent);
                appCompatActivity.finish();


            }
        });
        btn_Out.setAlpha(0.8f);


    }



    protected void postHttp(String URL , String LoginName , String LoginPwd){
        OkGo.<String>post(URL)
                .tag(this)
                .params("LoginName",LoginName)
                .params("LoginPwd",LoginPwd)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Toast.makeText(getContext(),response.body(),Toast.LENGTH_LONG).show();
                        Toast.makeText(getContext(),response.headers().get("Set-Cookie"),Toast.LENGTH_LONG).show();

                    }
                });
    }


    protected void getInformationForHttp(){
        OkGo.<String>get(SELECT_GRADE)
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        Document document = Jsoup.parse(response.body());
                        Elements Content = document.select("div.Content");

                        //获取姓名
                        Elements Name = Content.select("div[style=\"width:100px;\"]");
                        String temp ;
                        temp = Name.text();
                        temp = temp.replace("姓名：","");
                        info_name.setText(temp);

                        //获取学号
                        Elements Number = Content.select("div[style=\"width:170px;\"]");
                        String temp1 ;
                        temp1 = Number.text().replace("学号：","");
                        info_number.setText(temp1);

                        //获取学院信息
                        Elements School = Content.select("div[style=\"width:260px;\"]");
                        String temp2 =School.text().replace("学院：","");
                        info_school.setText(temp2);

                        //获取班级信息
                        Element Class = Content.select("div[style=\"width:180px;\"]").first();
                        String temp3 = Class.text().replace("班级：","");
                        info_class.setText(temp3);

                        //获取学制
                        Elements XZ = Content.select("div[style=\"width:120px;\"]");
                        String temp4 =XZ.text().replace("学制：","");
                        info_xz.setText(temp4);
                    }
                });














    }
}
